**Features**
1. Obtain the continuous values from light sensor and show them on the screen
2. Choose to play one of the 3 tones according to the current sensor’s value
3. The app keeps playing the tone even if the user does not interact with the screen for 1 minute

**How to run this application**
![Screenshot_2016-05-18-13-14-41.png](https://bitbucket.org/repo/okMEq4/images/1304139564-Screenshot_2016-05-18-13-14-41.png)
Launch the application. The numeric value under “Light sensor” represents the current output value from light sensor. Listen to the different tones under conditions with different light intensity.
Output value from light sensor

**Frequency of output tone**
< 100
200 Hz

>= 100 and < 250
300 Hz

>= 250
400 Hz


**Design & Implementation**
Obtain class Sensor representing device’s light sensor from SensorManager
Register listener to this sensor, detect when the value changes.
When value changes, update the value shown on the screen.
To ensure the tone does not change too rapidly, only generate tone after the last tone has been generated for more than 500 milliseconds.
Generate tone with the corresponding frequency and last for 300 milliseconds long

**Reference**
This application utilize the generateTone function from 
https://gist.github.com/slightfoot/6330866.